const express = require('express');
const app = express();
const port = 8000;
const secret = 'secretCode';

const Vigenere = require('caesar-salad').Vigenere;


app.get('/encode/:message', (req, res) => {
    const encodedMessage = Vigenere.Cipher(secret).crypt(req.params.message);
    res.send(`Encoded message: ${encodedMessage}`);
});


app.get('/decode/:encoded', (req, res) => {
    const decodedMessage = Vigenere.Decipher(secret).crypt(req.params.encoded);
    res.send(`Decoded message, ${decodedMessage}`);
});

app.listen(port, () => {
    console.log(` Hello Guys ${port}`);

});